# Observation Note

## Rule

post

-   post comments > 10
-   not school forums

comment

-   not hidden; hidden = true and content is null
-   have content; (TRIM(content) = '') is true

## Forum with echo chamber effect

these forum might have higher echo chamber effect in comments

-   pet
-   makeup
-   dressup
-   buyonline
-   relationship
-   girl
-   marriage
-   hairsalon
-   rainbow
-   entertainer
-   handicrafts
-   house
-   ecolife
-   photography
-   vehicle
-   horoscopesmarvel
-   food
-   travel
-   movie
-   tvepisode
-   music
-   game
-   acg
-   pokemon
-   sport
-   tennis
-   soccer
-   baseball
-   basketball
-   fitness
-   boy
-   3c
-   money
-   trending
-   job
-   studyabroad
-   literature
-   book
-   language
-   sex
